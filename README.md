https://www.youtube.com/playlist?list=PL6QREj8te1P7VSwhrMf3D3Xt4V6_SRkhu
https://github.com/adrianhajdin/project_mern_memories
https://github.com/adrianhajdin/project_mern_memories/tree/PART_6

# Docker
1. Create a docker file
2. Run this command and it will create an image from that docker file => docker build -t memories-image .
3. It will create an container => docker run -d --name {container name} {Docker image name} 
                               => docker run -d --name client memories-image
                               But this command did not connect it to conter port with local port
                               => docker run -d -p 3000:3000 --name client memories-image

# Docker-compose
1. If we have multiple docker file we can compose them in singel file command

    docker-compose up -d // To0 build first time
    docker-compose up -d --build //To build again