import React, { useState, useRef } from 'react';
import { Typography, TextField, Button } from '@material-ui/core/';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDispatch } from 'react-redux';
import { commentPost, deleteComments } from '../../store/actions/postsAction'
import useStyles from './styles';

const CommentSection = ({ post }) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const commentsRef = useRef()

    const [comment, setComment] = useState('');

    const [comments, setComments] = useState(post?.comments);

    const user = JSON.parse(localStorage.getItem('profile'));

    const handleComment = async () =>{
      const newComments =  await dispatch(commentPost(`${user?.result?.name}: ${comment}`, post._id));
      setComments(newComments);
      setComment('');

      commentsRef.current.scrollIntoView({ behavior: 'smooth' })
    }

    const deleteComment = async (commen) =>{
      const newComments = await dispatch(deleteComments(`${commen}`, post._id))
      setComments(newComments);
    }

    return (
        <div>
      <div className={classes.commentsOuterContainer}>
        <div className={classes.commentsInnerContainer} style={{ width: '100%' }}>
          <Typography gutterBottom variant="h6">Comments</Typography>
          {comments?.map((comment, i) => (
            <Typography key={i} gutterBottom variant="subtitle1">
            <div className="d-flex justify-content-between">
              <div>
                <strong>{comment.split(': ')[0]}</strong>
                {comment.split(':')[1]}
              </div>
              <div>{comment.split(': ')[0] === user?.result?.name && 
              <Button size="small" color="secondary" onClick={()=>deleteComment(comment)}>
                <DeleteIcon fontSize="small" />
              </Button>
              }</div>
            </div>
            </Typography>
          ))}
          <div ref={commentsRef} />
        </div>
        <div style={{ width: '70%' }}>
          <Typography gutterBottom variant="h6">Write a comment</Typography>
          <TextField fullWidth rows={4} variant="outlined" label="Comment" multiline value={comment} onChange={(e) => setComment(e.target.value)} />
          <br />
          <Button style={{ marginTop: '10px' }} fullWidth disabled={!comment.length} color="primary" variant="contained" onClick={handleComment}>
            Comment
          </Button>
        </div>
      </div>
    </div>
    )
}

export default CommentSection
