
import React, { useEffect } from 'react';
import { Paper, Typography, CircularProgress, Divider, Grid } from '@material-ui/core/';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { useParams, useHistory, Link } from 'react-router-dom';
import CommentSection from './CommentSection';
import { getPost, getPostsBySearch } from '../../store/actions/postsAction';
import useStyles from './styles';

const PostDetails = () => {
    const {post, posts, isLoading} =  useSelector(state => state.posts)
    const classes = useStyles();
    const dispatch = useDispatch();
    const { id } = useParams();
    const history = useHistory()

    useEffect(()=>{
        dispatch(getPost(id));
    }, [id])

    
    useEffect(() => {
        if (post) {
          dispatch(getPostsBySearch({ search: 'none', tags: post.tags.join(',') }));
        }
      }, [post]);

    if (!post) return null;
    
    const openPost = (_id) => history.push(`/posts/${_id}`);

    if (isLoading) {
      return (
        <Paper elevation={6} className={classes.loadingPaper}>
          <CircularProgress size="7em" />
        </Paper>
      );
    }

    const recommendedPosts = posts.filter((post) => post._id !== id)
    
    return (
        <Paper style={{ padding: '20px', borderRadius: '15px' }} elevation={6}>
        <div className={classes.card}>
          <div className={classes.section}>
            <Typography variant="h3" component="h2">{post.title}</Typography>
            <Typography gutterBottom variant="h6" color="textSecondary" component="h2">{post.tags.map((tag, index) => (
              <Link to={`/tags/${tag}`} style={{ textDecoration: 'none', color: '#3f51b5' }} key={index}>
                {` #${tag} `}
              </Link>
            ))}
            </Typography>
            <Typography gutterBottom variant="body1" component="p">{post.message}</Typography>
            <div className="d-flex justify-content-between">
                <Typography variant="h6">
                  Created by:
                  <Link to={`/creators/${post.name}`} style={{ textDecoration: 'none', color: '#3f51b5' }}>
                    {` ${post.name}`}
                  </Link>
                </Typography>
                <Typography variant="body1">{moment(post.createdAt).fromNow()}</Typography>
            </div>
            <Divider style={{ margin: '20px 0' }} />
            <Typography variant="body1"><strong>Realtime Chat - coming soon!</strong></Typography>
            <Divider style={{ margin: '20px 0' }} />
            <CommentSection post={post} />
            <Divider style={{ margin: '20px 0' }} />
          </div>
          <div className={classes.imageSection}>
            <img className={classes.media} src={post.selectFile || `https://cdn.wallpapersafari.com/31/11/J0hpO8.jpg`} alt={post.title} />
          </div>
        </div>
        {!!recommendedPosts.length && (
          <div className={classes.section}>
            <Typography gutterBottom variant="h5">You might also like:</Typography>
            <Divider />
            <Grid lg={12} className={classes.recommendedPosts}>
              {recommendedPosts.map(({ title, name, message, likes, selectFile, _id }) => (
                <Grid lg={2} onClick={() => openPost(_id)} key={_id}>
                  <Paper elevation={6} style={{ margin: '20px', cursor: 'pointer' }}>
                    <img src={selectFile || `https://cdn.wallpapersafari.com/31/11/J0hpO8.jpg`} width="100%" height="150px" alt={title}/>
                    <div style={{padding: '10px'}}>
                      <Typography gutterBottom variant="h6">{title}</Typography>
                      <Typography gutterBottom variant="subtitle2">{name}</Typography>
                      <Typography gutterBottom variant="subtitle2">{message.split('').splice(0, 40).join('')}...</Typography>
                      <Typography gutterBottom variant="subtitle1">Likes: {likes.length}</Typography>
                    </div>
                  </Paper>
                </Grid>
              ))}
            </Grid>
          </div>
        )}
      </Paper>
    )
}

export default PostDetails
