import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Card, CardContent, Button, Typography } from '@material-ui/core/';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import DeleteIcon from '@material-ui/icons/Delete';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ThumbUpAltOutlined from '@material-ui/icons/ThumbUpAltOutlined';
import moment from 'moment';
import { deletePost, likePost } from '../../../store/actions/postsAction';

import useStyles from './styles';

const Post = ({post, setCurrentId}) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const user = JSON.parse(localStorage.getItem('profile'));
    const [likes, setLikes] = useState(post?.likes);
    const history = useHistory();

    const userId = user?.result.googleId || user?.result?._id;
    const hasLikedPost = post.likes.find((like) => like === userId);

    const handleLike = async () => {
      dispatch(likePost(post._id));
  
      if (hasLikedPost) {
        setLikes(post.likes.filter((id) => id !== userId));
      } else {
        setLikes([...post.likes, userId]);
      }
    };

    const Likes = () => {
      if (likes.length > 0) {
        return likes.find((like) => like === userId)
          ? (
            <><ThumbUpAltIcon fontSize="small" />&nbsp;{likes.length > 2 ? `You and ${likes.length - 1} others` : `${likes.length} like${likes.length > 1 ? 's' : ''}` }</>
          ) : (
            <><ThumbUpAltOutlined fontSize="small" />&nbsp;{likes.length} {likes.length === 1 ? 'Like' : 'Likes'}</>
          );
      }
      return <><ThumbUpAltOutlined fontSize="small" />&nbsp;Like</>;
    };

    const openPost = () =>{
      history.push(`/posts/${post._id}`)
    }

    return (
      <div>
        <Card className={classes.card} raised elevation={12}>
         <div onClick={openPost} style={{ cursor: 'pointer' }}>
            <img src={post.selectFile || `https://cdn.wallpapersafari.com/31/11/J0hpO8.jpg`} alt={post.title} width='100%' height='170px'/>
         
           <div className="px-3 pt-3" >
             <div className="d-flex justify-content-between">
                <div className={classes.overlay}>
                  <Typography className={classes.title} gutterBottom variant="h5" component="h3">{post.title}</Typography>
                </div>
                {(user?.result?.googleId === post?.creator || user?.result?._id === post?.creator) && (
                <div className={classes.overlay2} name="edit">
                  <Button
                    onClick={(e) => {
                      e.stopPropagation(); //it get call once
                      setCurrentId(post._id);
                    }}
                    size="small"
                  >
                    <MoreHorizIcon fontSize="default" />
                  </Button>
                </div>
                )}
             </div>
              <div className={`${classes.details} d-flex justify-content-between`}>
                <Typography variant="body2" color="textSecondary" component="h2">{post.tags.map((tag) => `#${tag} `)}</Typography>
                <Typography variant="body2">{moment(post.createdAt).fromNow()}</Typography>
              </div>
           </div>
              
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p">{post.message.split('').splice(0, 80).join('')}...</Typography>
            </CardContent>
        
         </div>
        <div className="d-flex justify-content-between mb-2 px-2">
          <Button size="small" color="primary" onClick={handleLike} disabled={!user?.result}  >
            <Likes />
          </Button>
          {(user?.result?.googleId === post?.creator || user?.result?._id === post?.creator) && (
            <Button size="small" color="secondary" onClick={()=>{dispatch(deletePost(post._id))}}>
              <DeleteIcon fontSize="small" /> &nbsp; Delete
            </Button>
           )}
        </div>
      </Card>
      </div>
    )
}

export default Post;