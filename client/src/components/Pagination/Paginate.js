import React, { useEffect } from 'react';
import { Pagination, PaginationItem } from '@material-ui/lab';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import useSty from './styles';
import { getPosts } from '../../store/actions/postsAction';
const Paginate = ({ page }) => {
	const classes = useSty();
	const dispatch = useDispatch()
	const { currentPage, numberOfPages }  = useSelector( state => state.posts )
	useEffect(() => {
		if(page) dispatch(getPosts(page));
	}, [page, currentPage, dispatch])


	return (
		<Pagination
			classes={{ ul: classes.ul }}
			count={numberOfPages || 1}
			page={Number(currentPage) || 1}
			variant="outlined"
			color="primary"
			renderItem={(item) => (
				<PaginationItem
					{...item}
					component={Link}
					to={`/posts?page=${item.page}`}
				/>
			)}
		/>
	);
};

export default Paginate;
