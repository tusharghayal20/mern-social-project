import React, { useState, useEffect } from 'react'
import FileBase from 'react-file-base64';
import { TextField, Button, Typography, Paper } from '@material-ui/core';
// import ChipInput from 'material-ui-chip-input';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import { createPost, updatePost } from '../../store/actions/postsAction'
import useStyles from './styles';

const Form = ({currentId, setCurrentId}) => {
    const [ postData, setPostData ] = useState({ title: '', message: '', tags: '', selectFile: ''})
    const post = useSelector(state => currentId ? state.posts.posts.find( post => post._id === currentId): null)
    const dispatch = useDispatch()
    const classes = useStyles()
    const user = JSON.parse(localStorage.getItem('profile'));
    const history = useHistory()
    /********************************
     * Set post for update
     *******************************/
    useEffect(()=>{
        post && setPostData(post)
    }, [post])

    /********************************
     * Create New post Or update post
     *******************************/
    const handleSubmit = (e) =>{
        e.preventDefault()
        if (currentId) {
            dispatch(updatePost(currentId, {...postData, name: user?.result?.name }))
        }else{
            dispatch(createPost({...postData, name: user?.result?.name }, history))
        }
        clear()
    }

    /********************************
     * Input handelear
     *******************************/
    const onChangeHandelar = (e) =>{
        if (e.target.name === 'tags') {
            setPostData({
                ...postData,
                [e.target.name]: e.target.value.trim().split(',')
            })
        } else {
            setPostData({
                ...postData,
                [e.target.name]: e.target.value
            })
        }
    }

    /********************************
     * Clear form inputs
     *******************************/
    const clear = () =>{
        setCurrentId(null)
        setPostData({ title: '', message: '', tags: '', selectFile: ''})
    }

    if (!user?.result?.name) {
        return (
          <Paper className={classes.paper} elevation={6}>
            <Typography variant="h6" align="center">
              Please Sign In to create your own memories and like other's memories.
            </Typography>
          </Paper>
        );
      }
    
    return (
        <Paper className={classes.paper} elevation={6}>
            <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
                <Typography variant="h6">{currentId ? 'Editing' : 'Creating'} a Memory</Typography>

                {/* <TextField name="creator" variant="outlined" label="creator" fullWidth value={postData.creator} onChange={(e)=>onChangeHandelar(e)}/> */}
                <TextField name="title" variant="outlined" label="Title" fullWidth value={postData.title} onChange={(e)=>onChangeHandelar(e)}/>
                <TextField name="message" variant="outlined" label="Message" fullWidth multiline rows={4} value={postData.message} onChange={(e)=>onChangeHandelar(e)} />
                <TextField name="tags" variant="outlined" label="tags add with comma" fullWidth value={postData.tags} onChange={(e)=>onChangeHandelar(e)} />
                {/* <div style={{ padding: '5px 0', width: '94%' }}>
                    <ChipInput name="tags" variant="outlined" label="Tags" fullWidth />
                </div> */}
                <TextField name="selectFile" variant="outlined" label="Paste image url" fullWidth value={postData.selectFile} onChange={(e)=>onChangeHandelar(e)} />
                {/* <div className={classes.fileInput}><FileBase type="file" multiple={false}  onDone={({base64})=>setPostData({...postData, selectFile: base64 })}/></div> */}
                <Button className={classes.buttonSubmit} variant="contained" color="primary" size="large" type="submit" fullWidth>{currentId ? 'Update' : 'Submit'}</Button>
                <Button variant="contained" color="secondary" size="small"  fullWidth onClick={clear}>Clear</Button>
            </form>
        </Paper>
    )
}

export default Form
