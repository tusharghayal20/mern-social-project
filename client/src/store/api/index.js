import axios from 'axios';

const API = axios.create({ baseURL: 'http://localhost:5000'})

//Add header token
API.interceptors.request.use((req) => {
  if (localStorage.getItem('profile')) {
    req.headers.Authorization = `Bearer ${JSON.parse(localStorage.getItem('profile')).token}`;
  }
  return req;
});

/**************************
 * AUTH APIS
 **************************/
export const signIn = formData => API.post('/user/signin', formData);

export const signUp = formData => API.post('/user/signup', formData);

/**************************
 * POST APIS
 **************************/
export const fetchPosts = (page) => API.get(`/posts?page=${page}`);

export const fetchPost = (id) => API.get(`/posts/${id}`); //We have to fetch it by 
                                                          //req.params

export const fetchPostsBySearch = (searchQuery) => API.get(`/posts/search/all?searchQuery=${searchQuery.search || 'none'}&tags=${searchQuery.tags}`);
//We have to fetch it by  //req.query

export const createPosts = (newPost) => API.post('/posts', newPost);

export const updatePost = (id, updatedPost) => API.patch(`/posts/${id}`, updatedPost);

export const deletePost = (id, deletePost) => API.delete(`/posts/${id}`, deletePost);

export const likePost = (id) => API.patch(`/posts/likePost/${id}`, likePost);

export const comment = (commentData, id) => API.post(`/posts/commentPost/${id}`, {commentData});

export const deleteComment = (comment, id) => API.post(`/posts/deleteComment/${id}`, {comment});