import { FETCH_ALL, FETCH_POST, CREATE, UPDATE, DELETE, LIKE, FETCH_BY_SEARCH, START_LOADING, STOP_LOADING, COMMENT, DELETE_COMMENT } from '../constants/actionTypes';
import * as api from '../api';

//Action creators
export const getPosts = (page) => async (dispatch) => {
	try {
		dispatch({type: START_LOADING})
		const { data } = await api.fetchPosts(page);

		dispatch({ type: FETCH_ALL, payload: data });
		dispatch({type: STOP_LOADING})
	} catch (error) {
		console.error(error.message);
	}
};

//Action creators
export const getPost = (id) => async (dispatch) => {
	try {
		dispatch({type: START_LOADING})
		const { data } = await api.fetchPost(id);
		
		dispatch({ type: FETCH_POST, payload: data });
		dispatch({type: STOP_LOADING})
	} catch (error) {
		console.error(error.message);
	}
};

export const createPost = (newPost, history) => async (dispatch) => {
	try {
		dispatch({type: START_LOADING})
		const { data } = await api.createPosts(newPost);
		if (data) {
			dispatch({ type: CREATE, payload: data });
			history.push(`/posts/${data._id}`)
			dispatch({type: STOP_LOADING})
		}
	} catch (error) {
		console.error(error.message);
	}
};

export const updatePost = (id, post) => async (dispatch) =>{
	try {
		const { data } = await api.updatePost(id, post)
		dispatch({type: UPDATE, payload: data})
	} catch (error) {
		console.error(error.message);
	}
}

export const deletePost = ( id ) => async (dispatch) =>{
	try {
		await api.deletePost(id)
		dispatch({type: DELETE, payload: id})
	} catch (error) {
		console.error(error.message);
	}
}

export const likePost = (id) => async (dispatch) =>{
	try {
		const { data } = await api.likePost(id)
		dispatch({type: LIKE, payload: data})
	} catch (error) {
		console.error(error.message);
	}
}

export const getPostsBySearch = (searchQuery) => async (dispatch) => {
	try {
	  dispatch({ type: START_LOADING });
	  const { data: { data } } = await api.fetchPostsBySearch(searchQuery);

	  dispatch({ type: FETCH_BY_SEARCH, payload: { data } });
	  dispatch({type: STOP_LOADING})

	} catch (error) {
	  console.log(error);
	}
};

export const commentPost = (commentData, id) => async (dispatch) =>{
	try {
		const { data } = await api.comment(commentData, id);
		dispatch({type: COMMENT, payload: data})
		return data.comments;
	} catch (error) {
	console.error(error);
	}
}

export const deleteComments = (comment, id) => async (dispatch) =>{
	try {
		const { data } = await api.deleteComment(comment, id);
		dispatch({type: DELETE_COMMENT, payload: data})
		return data.comments;
	} catch (error) {
	console.error(error);
	}
}

/*
    return{
        type: 'FETCH_ALL',
        payload: []
    }
*/
// OR
/*
    const action = {
        type: 'FETCH_ALL',
        payload: []
    }

   dispatch(action);
*/
