import { combineReducers } from 'redux';
import posts from './postsReducer';
import auth from './authReducer';

const reducer = combineReducers({
	posts,
	auth
});

export default reducer