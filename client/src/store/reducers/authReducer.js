import { AUTH, LOGOUT } from '../constants/actionTypes'
const initialState = {
    authData: null
}

const auth = (state = initialState , action) =>{
    switch (action.type) {
        case AUTH:
            // console.log(action?.data);
            localStorage.setItem('profile', JSON.stringify({...action?.data}))
            return {
                ...state.auth,
                authData: action.data
            };

        case LOGOUT:
            localStorage.clear()// clear all storage
            return {
                ...state.auth,
                authData: null
            };
    
        default:
            return state;
    }
}

export default auth;
