const express = require('express');
const router = express.Router();
const auth = require('../middelware/auth')
const posts = require('../controllers/post.js');
// router.get('/' , posts.getPosts); // default '/posts'  //OR

const { getPosts, getPost, createPost, updatePost, deletePost, likePost, getPostsBySearch, commentPost, deleteComment } = posts
router.get('/' , getPosts); // default '/posts'
router.get('/:id', getPost); 
router.get('/search/all', getPostsBySearch);


router.post('/' , auth, createPost);
router.patch('/:id' , auth, updatePost);
router.delete('/:id' , auth, deletePost);
router.post('/deleteComment/:id' , auth, deleteComment);
router.patch('/likePost/:id' , auth, likePost);
router.post('/commentPost/:id', auth, commentPost)
module.exports = router