const express = require('express');
const router = express.Router();
const users = require('../controllers/user.js')
const auth = require('../middelware/auth')
const { signin, signup } = users

router.post('/signin', signin)
router.post('/signup', signup)

module.exports = router