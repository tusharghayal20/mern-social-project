const express = require('express');
const bodyParser = require('body-Parser');
const cors = require('cors');
const mongoose = require('mongoose');

const postRoutes = require('./routes/posts') 
const userRoutes = require('./routes/users.js') 

const app = express()


app.use(bodyParser.json({ limit: '30mb', extended: true})); //for large payload size
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true}));

app.use(cors())

app.use('/posts', postRoutes)
app.use('/user', userRoutes)
app.get("/server", (req, res)=>{
    console.log("HHHHHI");
    res.send("<h2>Hi</h2>");
})

// const CONNECTION_URL = 'mongodb+srv://Tushar:Tushar123@game.uhx0b.mongodb.net/memories?retryWrites=true&w=majority'
const CONNECTION_URL = `mongodb://root:123456@mongo:27017/?authSource=admin`
const PORT = process.env.PORT || 5000;
 
mongoose.connect( CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true }) //Connect to database
    .then(() => app.listen(PORT, ()=> console.log(` Server running on port ${PORT}`)))
    .catch((err)=> console.log(err.message))

mongoose.set('useFindAndModify', false); //remove warning

