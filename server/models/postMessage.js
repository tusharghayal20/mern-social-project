const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const postSchema = mongoose.Schema({
    title: String,
    message: String,
    name: String,
    creator: String,
    tags: [String],
    selectFile: String,
    likes: {
        type: [String],
        default: [],
    },
    comments: {
        type: [String],
        default: [],
    },
    createdAt: {
        type: Date,
        default: new Date(),
    },
})

postSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('PostMessage', postSchema);

// var PostMessage = mongoose.model('PostMessage', postSchema);

// module.exports = PostMessage;