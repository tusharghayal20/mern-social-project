const User = require('../models/user.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const secret = 'test';
exports.signin = async (req, res) => {
	const { email, password } = req.body;
	try {
		//find user
		const existingUser = await User.findOne({ email });
		//check user
		if (!existingUser)
			return res.status(404).json({ message: "User dosen't exist" });
		//check password
		const isPasswordCorrect = await bcrypt.compare(
			password,
			existingUser.password
		);

		if (!isPasswordCorrect)
			return res.status(400).json({ message: 'Invalid credentials.' });
		//create token
		const token = jwt.sign({ email: existingUser.email, id: existingUser._id }, secret, { expiresIn: "1h" });
		
		//send user and token
		res.status(200).json({ result: existingUser, token });
	} catch (error) {
		res.status(500).json({ message: 'Somthing went wrong' });
	}
};

exports.signup = async (req, res) => {
	const { firstName, lastName, email, password, confirmPassword } = req.body;
	try {
		//find user
		const existingUser = await User.findOne({ email });
		//check user
		if (existingUser)
			return res.status(400).json({ message: 'User already exist' });
		//compair password
		if (password !== confirmPassword)
			return res.status(400).json({ message: 'password dont match' });

		const hashedPassword = await bcrypt.hash(password, 12);

		const result = await User.create({
			email,
			password: hashedPassword,
			name: `${firstName} ${lastName}`,
		});

		//create token
		const token = jwt.sign({ email: result.email, id: result._id }, 'test', {
			expiresIn: '1h',
		});

		console.log(result, token);
		res.status(200).json({ result, token });
	} catch (error) {
		res.status(500).json({ message: 'Somthing went wrong' });
	}
};
