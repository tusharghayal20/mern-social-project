const mongoose = require('mongoose');
const PostMessage = require('../models/postMessage')

exports.getPosts = async (req, res) => {
    const { page } = req.query;

    console.log("page",page);
    
    try {
        const LIMIT = 8;
        const startIndex = (Number(page) - 1) * LIMIT; // get the starting index of every page
    
        const total = await PostMessage.countDocuments({});
        const posts = await PostMessage.find().sort({ _id: -1 }).limit(LIMIT).skip(startIndex);
        
        res.json({ data:posts, currentPage: Number(page), numberOfPages: Math.ceil(total / LIMIT)});
    } catch (error) {    
        res.status(404).json({ message: error.message });
    }
}

exports.getPost = async (req, res) => {
    const { id } = req.params;

    try {
        const post = await PostMessage.findById(id);
        res.status(200).json(post);
    } catch (error) {    
        res.status(404).json({ message: error.message });
    }
}

// exports.getPosts = async(req, res)=>{
//     try {
//         const postMessages = await PostMessage.find();
//         res.status(200).json(postMessages);
//     } catch (error) {
//         res.status(404).json({ message: error.message })
//     }
// }

exports.getPostsBySearch = async (req, res) => {
    const { searchQuery, tags } = req.query;
    
    try {
        const title = new RegExp(searchQuery, "i");

        const posts = await PostMessage.find({ $or: [ { title }, { tags: { $in: tags.split(',') } } ]});

        res.json({ data: posts });
    } catch (error) {    
        res.status(404).json({ message: error.message });
    }
}

exports.createPost = async (req, res)=>{
    try {
        const post = req.body;
        const newPostMessage = new PostMessage({ ...post, creator: req.userId, createdAt: new Date().toISOString() })
        await newPostMessage.save();
        res.status(201).json(newPostMessage);
    } catch (error) {
        res.status(409).json({ message: error.message });
    }
}

exports.updatePost = async (req, res) =>{
        //{ id: _id } Rename id to_id
    const { id: _id } = req.params;
    const post = req.body;
    //Check _id is mongoose object id or not
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No post with that id')  //404 Not Found
    try {
        const updatedPost = await PostMessage.findByIdAndUpdate(_id, post, {new: true})
        res.status(200).json(updatedPost)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}


exports.deletePost = async (req, res) =>{
    try {
        const { id: _id } = req.params;
        if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No post with that id') //404 Not Found
        await PostMessage.findByIdAndDelete(_id)
        res.status(200).json({ message: 'Post deleated Successfully'});
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

exports.likePost = async (req, res) => {
    const { id } = req.params;
    
    //Check if the user is authenticated or not
    //Get user id from auth //req.userId from auth
    if (!req.userId) {
        return res.json({ message: "Unauthenticated" });
      }

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);
    
    const post = await PostMessage.findById(id);

    const index = post.likes.findIndex((id) => id ===String(req.userId));
    if (index === -1) {
      post.likes.push(req.userId);//req.userId from auth
    } else {
      post.likes = post.likes.filter((id) => id !== String(req.userId));
    }

    const updatedPost = await PostMessage.findByIdAndUpdate(id, post, { new: true });

    res.status(200).json(updatedPost);
}

exports.commentPost = async (req, res) => {
    const { id } = req.params
    const { commentData } = req.body

    const post = await PostMessage.findById(id);
    post.comments.push(commentData)

    const updatedPost =  await PostMessage.findByIdAndUpdate( id, post, { new: true });

    res.status(200).json(updatedPost);
}

exports.deleteComment = async (req, res) => {
    const { id } = req.params
    const { comment } = req.body

    const post = await PostMessage.findById(id);
    const index = post.comments.indexOf(comment);
    post.comments.splice(index , 1);
    
    const updatedPost =  await PostMessage.findByIdAndUpdate( id, post, { new: true });
    res.status(200).json(updatedPost);
}